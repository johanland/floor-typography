// const presetEnv = require('postcss-preset-env')({
//   features: {
//     // enable nesting
//     'nesting-rules': true,
//   },
// });
const atImport = require('postcss-import');
const cssnano = require('cssnano');

const plugins = process.env.NODE_ENV === 'production' ?
	[atImport, cssnano] : [atImport];
	// [atImport, cssnano({ preset: 'default', })] : [atImport];

module.exports = { plugins };
