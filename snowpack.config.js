// Snowpack Configuration File
// See all supported options: https://www.snowpack.dev/#configuration

/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
	mount: {
		'web_src': '/dist',
		'web_public': {url: '/', static: true,},
		'doc': '/',
		'src': '/src',
	},
	plugins: [
		'@snowpack/plugin-svelte',
		'@snowpack/plugin-webpack',
		[
			'snowpack-plugin-raw-file-loader', {
				exts: ['.md'],
			},
		],
		'@snowpack/plugin-postcss',
	],
	buildOptions: {
		out: '_site',
	},
	// optimize: {
	// 	bundle: true,
	// 	minify: true,
	// 	target: 'es2018',
	// },
};
