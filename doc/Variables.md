# Variables

## Spacing

### `--spacer`

`--spacer` can be set to control most vertical spacings, e.g., top and bottom margins' of block elements, and top and bottom margins of headings.

Example:

```css
:root {
	--RLH: calc(1rem * (1 + var(--added-lead))); /* Root Line-Height (CONSTANT) */
	--spacer: var(--RLH); /* Adjusts vertical rhythm */
}
```

With Floor Typography CSS it is recommended to either use rem for `--spacer`, and rather adjust the `:root` `font-size` to the biggest body text for the app/site, then adjust surrounding text to be smaller if needed. Or (if needed) override `--spacer` on a deeper wrapping element in the DOM:

```css
:root {
	--root-spacer: 1rem;
	--spacer: var(--root-spacer);
}

article {
	--spacer: calc(var(--root-spacer) * 1.2);
	font-size: 1.2em;
}
```

*If you want to set `--spacer` to `1em`, margins of headings will grow quite large and you should probably not include the headings-margin.css file.*

## Line-Heights & Font-Sizes

### Root

`--added-lead` can be overridden to adjust `:root { line-height ... }`. It's the additional vertical spacing of `line-height` + 1.

Example of adjusting the line-height:

```css
:root {
	--added-lead: .25; /* To set line-height to 1.5 (default is 1.6 from vars.css) */
}

article {
	--added-lead: .5;
	line-height: calc(1 + var(--added-lead)); /* Must be set since initial line-height is set on root with var */
}
```

### Headings

You can adjust `font-size` and `line-height` of headings with the variables `h[1,2,3,4,5,6]-min-font-size-min` and `h[1,2,3,4,5,6]-added-lead`.

Example:

```css
:root {
	--h1-added-lead: .2;
	--h2-added-lead: .2;
}

@media (min-width: 1200px) {
	:root {
		--h3-added-lead: .2;
	}
}

@media (min-width: 1500px) {
	:root {
		/* Huge h1 on XL screens. */
		--h1-min-font-size: 4em;
	}
}
```

## Font-Families

### All Headings

The `--headings-font` variable can be set to control the `font-family` of all headings.

Example:

```css
:root {
	--font-system: system-ui, -apple-system, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
	--headings-font: var(--font-system);
}
```

### Heading Level

Font families for each heading can be set by variables `--h1-font`, `--h2-font`, `--h3-font`, `--h4-font`, `--h5-font` and `--h6-font`.

### Mono Font

The `--mono-font` variable can be set to control the `font-family` of all mono-font elements (`pre`, `code`, `kbd`, `samp`).
