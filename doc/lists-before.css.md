```css
/** List-Style Before */

ol, ul {
	--indent: var(--spacer, 1rem);
	list-style: none;
	padding-inline-start: var(--indent);
}

li {
	margin: var(--spacer, 1rem) 0;
	text-indent: calc(var(--indent) * -.5);
}

li::before {
	display: inline-block;
	min-width: calc(var(--indent) * .5);
	text-align: end;
	color: gray;
	font-weight: bold;
}

ol {
	counter-reset: ol-counter;
}

ol > li::before {
	counter-increment: ol-counter;
	content: counter(ol-counter) ".\0000a0"; /* \0000a0 is space */
}

ul > li::before {
	content: "\2022\0000a0"; /* \2022 is bullet */
}

/* Fix text-indent side effect on child elements. */
li :not(li) {
	text-indent: 0;
}

/* Fix first-child block elements (would start on new line). */
li > :first-child:not(ol):not(ul),
li > * > :first-child:not(ol):not(ul):not(li) {
	display: inline;
}
```
