```css
/** List-Style Marker */

ol, ul {
	--indent: var(--spacer, 1rem);
	padding-inline-start: var(--indent);
}

li {
	margin: var(--spacer, 1rem) 0;
}

li::marker {
	color: gray;
	font-weight: bold;
}

ul {
	list-style: none;
}

ul > li::marker {
	content: "\2022\0000a0"; /* \2022 is bullet */
}

ol > li {
	text-indent: calc(var(--indent) * -1);
}

/* Fix text-indent side effect on child elements. */
ol li :not(li) {
	text-indent: 0;
}

/* Fix first-child block elements (would start on new line). */
ol > li > :first-child:not(ol):not(ul),
ol > li > * > :first-child:not(ol):not(ul):not(li) {
	display: inline;
}
```
