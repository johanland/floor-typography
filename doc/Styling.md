# Styling

*Floor Typography CSS is the floor for your base/theme style, and does not contain much styling. An excerpt below of the quite minimal CSS for this site.*

## Headings

<p class="h1">Heading One (h1)</p>

<p class="h2">Heading Two (h2)</p>

<p class="h3">Heading Three (h3)</p>

<p class="h4">Heading Four (h4)</p>

<p class="h5">Heading Five (h5)</p>

<p class="h6">Sixth Heading (h6)</p>

---

## Lists

- item
- item
	1. Item
	1. Item
		- Item
		- Item
		- Item
	1. Item
- item

----

## `sub` & `sub`

```css
/* (In your global stylesheet) */
/* Normalizing `sub` and `sup`: */
@import 'floor-typography-css/src/floor-sub-sup.css';
```

<p>Almost every developer's favorite molecule is
C<sub>8</sub>H<sub>10</sub>N<sub>4</sub>O<sub>2</sub>, also known as "caffeine."<br aria-hidden><em>Normalized so `sub` does not affect line-heigh.</em></p>

<p>The <b>Pythagorean theorem</b> is often expressed as the following equation:</p>

<p><var>a<sup>2</sup></var> + <var>b<sup>2</sup></var> = <var>c<sup>2</sup></var><br aria-hidden><em>Normalized so `sup` does not affect line-heigh.</em></p>

## Example use

```css
@import 'floor-typography-css/src/floor.css';
@import 'floor-typography-css/src/floor-sub-sup.css';
@import 'floor-typography-css/src/headings.css';
@import 'floor-typography-css/src/headings-md.css';

:root {
	--body-gap-x: max(6vw, 1rem);
	--logo-height: .25ex;
	--mono-font: ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, Liberation Mono, Courier New, monospace; /* Mono elements' fonts are set to --mono-font */
	--code-bg: hsla(0 0% 50% / 12.5%);
	--font-size: clamp(1rem, 1rem + 0.55vw, 1.25em);

	font-family: ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
}

header, main {
	--spacer: var(--font-size);
	/* For block spacings to equal a line-height: */
	/* --spacer: calc(var(--font-size) * (1 + var(--added-lead, .6))); */

	font-size: var(--font-size);
}

@media (min-width: 900px) {
	:root {
		--body-gap-x: 15vw;
		--font-size: clamp(1rem, .5rem + 0.55vw, 1.12em);
	}
}

body {
	margin: 0;
	padding: 0 var(--body-gap-x) var(--font-size);
}

/** Site width */

article > *,
.site-footer {
	max-width: 50rem;
}

/** Headings */

h1, .h1 {
	margin-block-start: calc(3.5 * var(--spacer, 1rem));
	margin-block-end: calc(2.5 * var(--spacer, 1rem));
}

h2, .h2 {
	margin-block-start: calc(var(--spacer, 1rem) * 3);
	margin-block-end: calc(var(--spacer, 1rem) * 2);
}

h3, .h3 {
	margin-block-start: calc(var(--spacer, 1rem) * 1.5);
	margin-block-end: calc(var(--spacer, 1rem) * .5);
}

h4, .h4,
h5, .h5,
h6, .h6 {
	margin-block-end: 0;
}

h3 + :not(hr), .h3 + :not(hr),
h4 + :not(hr), .h4 + :not(hr),
h5 + :not(hr), .h5 + :not(hr),
h6 + :not(hr), .h6 + :not(hr) {
	margin-block-start: 0;
}
```
