# Files

All source-files are in the "src" folder.

<small>(CSS variables included per file in parentheses)</small>

## Floor

<details>
<summary>floor.css
<br aria-hidden="true"><small>(--added-lead, --mono-font, --spacer)</small>
</summary>

A slightly opinionated (but less than several) CSS browser-normalizer and a simple styling for more intuitive defaults and better defaults for narrow viewports. Of note:

- Consistent vertical spacing for [block-level elements](//developer.mozilla.org/en-US/docs/Web/HTML/Block-level_elements).
- Opposite of some other CSS normalizers/resets, `margin` for `body` is not removed/set.
- `:root` has `hyphens: auto;`. On large viewports you may want to set it to `manual` for certain elements.
- `pre` content is wrapped by default. Because of Safari this is an easier default – if `white-space: pre` would have been default, `overflow-wrap` would have to be set to `normal` to fix Safari only. To get a basic non-wrapping `pre`s for code, you need:

	```css
	pre {
		overflow-y: auto;
		overflow-wrap: normal;
		padding: var(--spacer, 1rem) 0;
		white-space: pre;
	}
	```

</details>

<details>
<summary>floor-sub-sup.css
</summary>

Normalize styling of `sub` and `sup` elements.
</details>

## Headings

<details>
<summary>headings.css
<br aria-hidden="true"><small>(--h1-min-font-size, --h2-min-font-size, --h3-min-font-size, --h4-min-font-size, --h5-min-font-size, --h6-min-font-size,
<br aria-hidden=true">--h1-added-lead, --h2-added-lead, --h3-added-lead, --h4-added-lead, --h5-added-lead, --h6-added-lead)</small>
</summary>

A simple typographic base style for all headings.

To reset headings, e.g., outside of `main` you could do something like this:

```css
.reset-headings h2,
.reset-headings h3 {
	margin-block-start: var(--spacer, 1rem);
	margin-block-end: var(--spacer, 1rem);
	font-family: var(--default-font, serif);
	font-size: 1em;
	line-height: calc(1 + var(--added-lead, .6));
}
```
</details>

<details>
<summary>headings-md.css
</summary>

Sets h1 min size to larger than h2. Can be imported for the media query you like, or as default – which is often the case, unless you have long h1 titles.
</details>

<details>
<summary>headings-lg.css
</summary>

The heading variables that sets bigger `h1`, `h2` and `h3`. The file can be imported for the media query you like.
</details>

<details>
<summary>headings-margin.css or headings-margin-lg.css
<br aria-hidden="true"><small>(--spacer)</small>
</summary>

Add one or the other for top and bottom margins of headings.
</details>

<details>
<summary>headings-font-family.css
<br aria-hidden="true"><small>(--headings-font, --h1-font, --h2-font, --h3-font, --h4-font, --h5-font, --h6-font)</small>
</summary>

This file add variables `--headings-font` and `h[1,2,3,4,5,6]-font` to control the font-families of headings and their class counterparts.
</details>

## More

<details>
<summary>visually-hidden.css</summary>

The classes `visually-hidden` and `visually-hidden-focusable` for screen reader only content and skip links that show on focus.
</details>
