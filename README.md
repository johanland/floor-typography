# Floor Typography CSS

*The floor you build style on* – Floor Typography CSS is an up-to-date normalizer with the additional minimum for an intuitive default CSS for typography and today’s web-app demands. *Licence: ISC*

**Explore the remake @[github.com/jssteinberg/some-makeup](https://github.com/jssteinberg/some-makeup)!**

- **Default CSS,** as simple as needed, with CSS variables.
- **Vanilla CSS** only, utilizing CSS functions and variables.
- **Typography ready,** with more typography-handy variables.
- **Bloat free.** Flexible, light and minimal.
- **Optional files** brings more default CSS for certain elements or scenarios.
- **Relative units** `em`, `rem` and `%`.


## npm

```sh
npm install floor-typography-css
```

Import the files you need – e.g., with [postcss-import](https://github.com/postcss/postcss-import) and Postcss – floor.css being the core file of Floor Typography CSS. Bundle for production. Once imported globally, Floor Typography CSS will set the default CSS.


## floor.css

```css
/* (In your global stylesheet) */
@import 'floor-typography-css/src/floor.css';
```

floor.css normalizes and sets intuitive CSS defaults, for narrow viewports first. Further, it provides consistent vertical spacing between the needed [block-level elements](//developer.mozilla.org/en-US/docs/Web/HTML/Block-level_elements), using CSS variables.

### Variable for line-height

`:root { --added-lead: .5; }` – a decimal number; is added to 1 for root line-height; falls back to `.6` – `:root` has line-height `calc(1 + var(--added-lead, .6))`.

### Variable for block (top/bottom) margins

`article { --spacer: calc(var(--font-size) * (1 + var(--added-lead, .6))); }` – use `rem` or a `rem` based calculation (e.g., with `clamp()`); adjusts vertical spacing of the necessary block-level elements; falls back to `1rem`.

### Variable for mono-font

`:root { --mono-font: courier new; }` – font variable that sets `font-family` for `pre, code, kbd, samp`; falls back to `monospace`.

### More “floor” Files

```css
/* (In your global stylesheet) */
/* Normalizing `sub` and `sup`: */
@import 'floor-typography-css/src/floor-sub-sup.css';
```


## headings.css

```css
/* (In your global stylesheet) */
@import 'floor-typography-css/src/headings.css';
```

headings.css (and related files) sets a simple, distinguishable styling for headings. Headings’ line-height are, in height, set to compute to root line-height(s) for a classically based typography.

The only classes provided with Floor Typography CSS are from the “headings” files – `h1`, `h2`, `h3`, `h4`, `h5` and `h6` – to style other elements as headings.

### Variables for font-size

Font-sizes for headings should be customized by the following variables when using this file. The actual font-size is calculated so headings line-height computes to the height of one, or more, normal lines of the nearest container with variable `--added-lead` (`:root` by default):

* `main { --h1-min-font-size: 2em; }` – (`--h[1,2,3,4,5,6]-min-font-size`). Variable, an integer `em` value, sets the minimum font size of the heading; falls back to `1em`.

* `main { --h1-added-lead: .1; }` – (`--h[1,2,3,4,5,6]-added-lead`). Variable, a decimal number, that will adjust line-height (and font size) the heading; falls back to `.125` for `h1`, `.25` for `h2`, and `--added-lead` for the rest.

### More “headings” Files

```css
/* (In your global stylesheet) */

/* Bigger h1: */
@import 'floor-typography-css/src/headings-md.css';

/* Bigger h1, h2 and h3: */
@import 'floor-typography-css/src/headings-lg.css' (min-width: 1200px);

/* More headings' block margin (also headings-margin-lg.css): */
@import 'floor-typography-css/src/headings-margin.css';

/* Variables `--headings-font` and `--h[1,2,3,4,5,6]-font` for setting font-families: */
@import 'floor-typography-css/src/headings-font-family.css';
```


## Related

*The Elements of Typographic Style* by Robert Bringhurst,
*[The Elements of Typographic Style: Applied to the Web](//webtypography.net/)*,
[text-rendering](https://css-tricks.com/almanac/properties/t/text-rendering),
[Google Fonts Knowledge](https://fonts.google.com/knowledge),
[Butterick’s Practical Typography](//practicaltypography.com),
[CSS Remedy](//github.com/jensimmons/cssremedy),
[Open Props](//open-props.style),
[sanitize.css](//github.com/csstools/sanitize.css),
[normalize.css](//github.com/necolas/normalize.css).
