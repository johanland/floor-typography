const gotoElement = (el) => {
	el.focus();
	el.scrollIntoView( {behavior: 'smooth'} );
};

export default gotoElement;
