import Index from './routes/index.svelte';
import { wrap } from 'svelte-spa-router/wrap';

const routes = {
	'/': Index,

	'/documentation/files': wrap({
		asyncComponent: () => import('./routes/documentation/files.svelte')
	}),
	'/documentation/variables': wrap({
		asyncComponent: () => import('./routes/documentation/variables.svelte')
	}),
	'/documentation/styling': wrap({
		asyncComponent: () => import('./routes/documentation/styling.svelte')
	}),

	'/snippets/lead': wrap({
		asyncComponent: () => import('./routes/snippets/lead.svelte')
	}),
	'/snippets/list-styles': wrap({
		asyncComponent: () => import('./routes/snippets/list-styles.svelte')
	}),
	'/snippets/space-line-height': wrap({
		asyncComponent: () => import('./routes/snippets/space-line-height.svelte')
	}),

	// Catch-all
	// This is optional, but if present it must be the last
	// '*': NotFound,
};

export default routes;
