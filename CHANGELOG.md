# Changelog

***Disclaimer.** The high version number reflects how the library has been experimental at times. It’s now landed at a 'stable floor', it’s used professionally, and development is at a mature stage.*

Plans: rename "--...-min-font-size" to "--...-line-span"?

Plans (when browser support is good enough):

- Use `:where` for low specificity.
- Use, e.g.: `hyphenate-limit-chars: 10 3 4;`
- This, from Open Props:

	```css
	:where(img, svg, video) {
		max-inline-size: 100%;
		block-size: auto;
	}
	```

__Version 22:__

*When floor became floor*

- **Breaking change:** Moved "headings-font" CSS files to "headings".
- **Breaking change:** Remove variable `--elements-spacer`. Set a global constant `--SPACER` variable, set `--spacer: var(--SPACER)`.
- **Breaking change.** Merge "normalize-style.css" and "reset.css" to a single file: "floor.css".
- **Breaking change.** Remove files: "normalize-audio.css", "lists-simple.css", "vars.css", "blockquote-simple.css", "table-simple.css", "headings-font-weight.css".
- Add hiding of `audio` with no controls to "floor.css".
- **Breaking change.** Rename files starting with "normalize" to "floor".
- **Breaking change.** Rename files starting with "classes-" to not start with that.
- **Breaking change.** Move `border-box` directly to all (pseudo) elements, not `inherit`:

	```css
	*, ::before, ::after { box-sizing: border-box; }
	```

- Remove `pointer` cursor from `<summary>`. Can be added in a base when wanted.


__Version 21:__

- Change CSS var names to follow order, including what's applicable, selector by specificity: `--selector-state-property-value`.
	- Change CSS var name `--h-font` to `--headings-font`.
- Revert to wrapping `pre` by default, as CSS remedy does it. Because of Safari, it's easier to normalize this way. If `pre`s should have `white-space: pre`, `overflow-wrap` must be set to `normal` for Safari only. To get a basic non-wrapping `pre`s for code, you need:

	```css
	pre {
		overflow-y: auto;
		overflow-wrap: normal;
		padding: var(--elements-spacer, var(--spacer, 1rem)) 0;
		white-space: pre;
	}
	```
